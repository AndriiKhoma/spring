package springboot.service;

import org.springframework.stereotype.Service;
import springboot.dao.ProductRepository;
import springboot.model.Product;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProduct() {
        List<Product> productList = new ArrayList<>();
        productRepository.findAll().forEach(productList::add);
        return productList;
    }

    public Product getProduct(int id) {
        return productRepository.findById(id).orElse(new Product());
    }

    public void addProduct(Product product) {
        productRepository.save(product);
    }

    public void updateProduct(Product product, int id) {
        productRepository.save(product);
    }

    public void deleteProduct(int id) {
       productRepository.deleteById(id);
    }
}


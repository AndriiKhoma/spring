package springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springboot.model.Product;

@Repository
public interface ProductRepository  extends CrudRepository<Product, Integer> {
}
